<?php

namespace Drupal\rabbitmq\Exception;

/**
 * Exception when for an invalid serializer.
 */
class InvalidSerializerException extends RuntimeException {
}
