<?php

namespace Drupal\rabbitmq\Serialization;

use Drupal\Component\Serialization\SerializationInterface;

/**
 * Interface for the RabbitMq Serializer collector.
 */
interface SerializerCollectorInterface {

  /**
   * Obtain a serializer.
   *
   * @param non-empty-string $serializer
   *   ServiceId of the serializer to retrieve.
   *
   * @return \Drupal\Component\Serialization\SerializationInterface
   *   The serializer.
   *
   * @throws \Drupal\rabbitmq\Exception\InvalidSerializerException
   *   If the Serializer is not registered or is not a Serializer.
   */
  public function getSerializer(string $serializer): SerializationInterface;

}
