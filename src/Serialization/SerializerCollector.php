<?php

declare(strict_types=1);

namespace Drupal\rabbitmq\Serialization;

use Drupal\Component\Serialization\SerializationInterface;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\rabbitmq\Exception\InvalidSerializerException;

/**
 * Collects serializer compatible with rabbitmq.
 */
final class SerializerCollector implements SerializerCollectorInterface {

  /**
   * Array of ServiceId's of known serializers.
   *
   * @var string[]
   */
  protected array $serializerIds = [];

  /**
   * Constructs a new SerializerCollector.
   *
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $classResolver
   *   The Class Resolver service.
   * @param string[] $serializer_ids
   *   Array of serializer ServiceId's.
   */
  public function __construct(
    protected ClassResolverInterface $classResolver,
    array $serializer_ids
  ) {
    foreach ($serializer_ids as $key => $value) {
      $this->serializerIds[$value] = $key;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getSerializer(string $serializer): SerializationInterface {
    if (!isset($this->serializerIds[$serializer])) {
      throw new InvalidSerializerException('Unknown Serializer');
    }

    $serializer = $this->classResolver->getInstanceFromDefinition($serializer);

    if (!$serializer instanceof SerializationInterface) {
      throw new InvalidSerializerException('Does not implement SerializationInterface');
    }

    return $serializer;
  }

}
