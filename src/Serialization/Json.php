<?php

declare(strict_types=1);

namespace Drupal\rabbitmq\Serialization;

use Drupal\Component\Serialization\Exception\InvalidDataTypeException;
use Drupal\Component\Serialization\SerializationInterface;

/**
 * JSON serialization with exceptions.
 *
 * Originally copied from drupal/core
 * \Drupal\Component\Serialization\Json.
 */
final class Json implements SerializationInterface {

  /**
   * {@inheritdoc}
   */
  public static function encode($data): string {
    try {
      return json_encode($data, JSON_THROW_ON_ERROR);
    }
    catch (\JsonException $e) {
      throw new InvalidDataTypeException($e->getMessage(), $e->getCode(), $e);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function decode($string): mixed {
    try {
      return json_decode($string, TRUE, 512, JSON_THROW_ON_ERROR);
    }
    catch (\JsonException $e) {
      throw new InvalidDataTypeException($e->getMessage(), $e->getCode(), $e);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getFileExtension() {
    return 'json';
  }

}
