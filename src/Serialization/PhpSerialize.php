<?php

declare(strict_types=1);

namespace Drupal\rabbitmq\Serialization;

use Drupal\Component\Serialization\Exception\InvalidDataTypeException;
use Drupal\Component\Serialization\SerializationInterface;

/**
 * PHP serialization with exceptions.
 *
 *  Originally copied from drupal/core
 *  \Drupal\Component\Serialization\PhpSerialize.
 */
final class PhpSerialize implements SerializationInterface {

  /**
   * {@inheritdoc}
   */
  public static function encode($data): string {
    return serialize($data);
  }

  /**
   * {@inheritdoc}
   */
  public static function decode($raw): mixed {
    // Any class may be passed through RabbitMq.
    // phpcs:ignore DrupalPractice.FunctionCalls.InsecureUnserialize.InsecureUnserialize
    $result = @unserialize($raw);

    if ($result !== FALSE) {
      return $result;
    }

    if ($raw === serialize(FALSE)) {
      return FALSE;
    }

    throw new InvalidDataTypeException('String was unserializable.');
  }

  /**
   * {@inheritdoc}
   */
  public static function getFileExtension() {
    return 'serialized';
  }

}
