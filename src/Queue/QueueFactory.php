<?php

namespace Drupal\rabbitmq\Queue;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Site\Settings;
use Drupal\rabbitmq\ConnectionFactory;
use Drupal\rabbitmq\Serialization\SerializerCollectorInterface;
use Psr\Log\LoggerInterface;

/**
 * Class RabbitMQ QueueFactory.
 */
class QueueFactory {

  const SERVICE_NAME = 'queue.rabbitmq';
  const DEFAULT_QUEUE_NAME = 'default';
  const MODULE_CONFIG = 'rabbitmq.config';

  /**
   * The config.factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The server factory service.
   *
   * @var \Drupal\rabbitmq\ConnectionFactory
   */
  protected $connectionFactory;

  /**
   * The logger service for the RabbitMQ channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The module_handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The UUID service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuid;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructor.
   *
   * @param \Drupal\rabbitmq\ConnectionFactory $connectionFactory
   *   The connection factory service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service for the RabbitMQ channel.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config.factory service.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   *   The UUID service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\rabbitmq\Serialization\SerializerCollectorInterface $serializerCollector
   *   The RabbitMq Serializer Collector service.
   */
  public function __construct(
    ConnectionFactory $connectionFactory,
    ModuleHandlerInterface $moduleHandler,
    LoggerInterface $logger,
    ConfigFactoryInterface $configFactory,
    UuidInterface $uuid,
    TimeInterface $time,
    protected readonly SerializerCollectorInterface $serializerCollector,
  ) {
    $this->configFactory = $configFactory;
    $this->connectionFactory = $connectionFactory;
    $this->logger = $logger;
    $this->moduleHandler = $moduleHandler;
    $this->uuid = $uuid;
    $this->time = $time;
  }

  /**
   * Constructs a new queue object for a given name.
   *
   * @param string $name
   *   The name of the Queue holding key and value pairs.
   *
   * @return Queue
   *   The Queue object
   */
  public function get(string $name): Queue {
    $moduleConfig = $this->configFactory->get(static::MODULE_CONFIG);
    $queue = new Queue($name, $this->connectionFactory, $this->moduleHandler, $this->logger, $moduleConfig, $this->uuid, $this->time, $this->serializerCollector);
    return $queue;
  }

  /**
   * To limit breakage, reset unavailable queue.rabbitmq to queue.database.
   */
  public static function overrideSettings(): void {
    // Regrettably, Settings can not be modified using the public core API.
    $settings = Settings::getInstance();
    $reflectionClass = new \ReflectionClass($settings);
    $reflectionProperty = $reflectionClass->getProperty('storage');
    $reflectionProperty->setAccessible(TRUE);
    $storage = $reflectionProperty->getValue($settings);
    unset($storage['queue_default']);
    $reflectionProperty->setValue($settings, $storage);
  }

}
