<?php

namespace Drupal\rabbitmq\Queue;

/**
 * Interface to define queue items containing (optional) properties.
 *
 * @see \Drupal\rabbitmq\Queue\Queue::createItem()
 */
interface QueueItemWithPropertiesInterface {

  /**
   * Obtain the data that is associated with the task in the queue.
   *
   * @return mixed
   *   Data associated with the queued item.
   */
  public function getData();

  /**
   * Obtain additional message-specific properties.
   *
   * @return array
   *   Properties that have to be included when placing the data on the queue.
   *   E.g., the delivery mode, property, etc. can be set.
   *
   * @see \PhpAmqpLib\Message\AMQPMessage
   */
  public function getProperties(): array;

}
