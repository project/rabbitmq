<?php

namespace Drupal\rabbitmq\Queue;

use Drupal\Core\Config\ConfigException;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\rabbitmq\ConnectionFactory;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Exception\AMQPProtocolChannelException;
use Psr\Log\LoggerInterface;

/**
 * Low-level Queue API implementation for RabbitMQ on top of AMQPlib.
 *
 * This class contains the low-level properties and methods not exposed by
 * the Queue API ReliableQueueInterface: those are implemented in Queue.php.
 *
 * @see \Drupal\rabbitmq\Queue\Queue
 */
abstract class QueueBase {

  const LOGGER_CHANNEL = 'rabbitmq';

  const MODULE = 'rabbitmq';

  /**
   * Object that holds a channel to RabbitMQ.
   *
   * @var \PhpAmqpLib\Channel\AMQPChannel|null
   */
  protected $channel;

  /**
   * The RabbitMQ connection service.
   *
   * @var \PhpAmqpLib\Connection\AbstractConnection
   */
  protected $connection;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The name of the queue.
   *
   * @var string
   */
  protected $name;

  /**
   * The queue options.
   *
   * @var array
   */
  protected $options;

  /**
   * A queue array: [writer, item count, consumer count].
   *
   * @var array|null
   */
  protected $queue;

  /**
   * The datetime service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructor.
   *
   * @param string $name
   *   The name of the queue to work with: an arbitrary string.
   * @param \Drupal\rabbitmq\ConnectionFactory $connectionFactory
   *   The RabbitMQ connection factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   * @param \Drupal\Core\Config\ImmutableConfig $moduleConfig
   *   The config.factory service.
   *
   * @throws \Drupal\Core\Config\ConfigValueException
   *   If the exchange and/or queue name is missing in the configuration.
   */
  public function __construct(
    string $name,
    ConnectionFactory $connectionFactory,
    ModuleHandlerInterface $moduleHandler,
    LoggerInterface $logger,
    ImmutableConfig $moduleConfig
  ) {
    $this->name = $name;
    $this->connection = $connectionFactory->getConnection();
    $this->moduleHandler = $moduleHandler;
    $this->logger = $logger;

    $this->options = ['name' => $name];

    // Retrieve queue options from the active storage, if present.
    $queues = $moduleConfig->get('queues');
    if ($queues) {
      foreach ($queues as $queue) {
        if (empty($queue['name'])) {
          throw new ConfigException('Queue name is missing in configuration.');
        }

        if ($queue['name'] === $name) {
          $this->options += $queue;
          break;
        }
      }
    }

    // Declare any exchanges required if configured.
    $exchanges = $moduleConfig->get('exchanges');
    if ($exchanges) {
      foreach ($exchanges as $exchange) {
        if (empty($exchange['name'])) {
          throw new ConfigException('Exchange name is missing in configuration.');
        }

        $this->connection->channel()->exchange_declare(
          $exchange['name'],
          $exchange['type'] ?? 'direct',
          $exchange['passive'] ?? FALSE,
          $exchange['durable'] ?? TRUE,
          $exchange['auto_delete'] ?? FALSE,
          $exchange['internal'] ?? FALSE,
          $exchange['nowait'] ?? FALSE
        );
      }
    }
  }

  /**
   * Obtain an initialized channel to the queue.
   *
   * @return \PhpAmqpLib\Channel\AMQPChannel
   *   The queue channel.
   */
  public function getChannel(): AMQPChannel {
    if (FALSE === isset($this->channel)) {
      $this->channel = $this->connection
        ->getConnection()
        ->channel();

      // Initialize a queue on the channel.
      $this->getQueue($this->channel);
    }

    return $this->channel;
  }

  /**
   * Declare a queue and obtain information about the queue.
   *
   * @param \PhpAmqpLib\Channel\AMQPChannel $channel
   *   The queue channel.
   * @param array $optionsOverrides
   *   Options overriding the queue defaults.
   *
   * @return mixed|null
   *   Not strongly specified by php-amqplib. Expected to be a 3-item array:
   *   - ProtocolWriter
   *   - Number of items
   *   - Number of clients
   */
  protected function getQueue(AMQPChannel $channel, array $optionsOverrides = []) {
    if (FALSE === isset($this->queue)) {
      // Declare the queue.
      $options = array_merge($this->options, $optionsOverrides);
      try {
        $this->queue = $channel->queue_declare(
          $this->name,
          $options['passive'] ?? FALSE,
          $options['durable'] ?? TRUE,
          $options['exclusive'] ?? FALSE,
          $options['auto_delete'] ?? TRUE,
          $options['nowait'] ?? FALSE,
          $options['arguments'] ?? NULL,
          $options['ticket'] ?? NULL
        );
      }
      catch (AMQPProtocolChannelException $e) {
        return NULL;
      }

      // Bind the queue to an exchange if defined.
      if ($this->queue && !empty($options['routing_keys'])) {
        foreach ($options['routing_keys'] as $routingKey) {
          [$exchange, $key] = explode('.', $routingKey, 2);
          $this->channel->queue_bind($this->name, $exchange, $key);
        }
      }
    }

    return $this->queue;
  }

  /**
   * Shutdown.
   */
  public function shutdown(): void {
    if ($this->channel) {
      $this->channel->close();
    }
    if ($this->connection) {
      $this->connection->close();
    }
  }

}
