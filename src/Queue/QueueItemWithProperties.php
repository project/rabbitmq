<?php

namespace Drupal\rabbitmq\Queue;

/**
 * Defines queue items that contain (optional) properties.
 *
 * @see \Drupal\rabbitmq\Queue\Queue::createItem()
 */
class QueueItemWithProperties implements QueueItemWithPropertiesInterface {

  /**
   * The data that is associated with the task in the queue.
   *
   * @var mixed
   */
  private $data;

  /**
   * Additional message-specific properties.
   *
   * @var array
   */
  private $properties = [];

  /**
   * Constructs a new QueueItemWithProperties object.
   *
   * @param mixed $data
   *   The data that is associated with the task in the queue.
   * @param array $properties
   *   (optional) Additional message specific properties.
   */
  public function __construct($data, array $properties = []) {
    $this->data = $data;
    $this->properties = $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function getData() {
    return $this->data;
  }

  /**
   * {@inheritdoc}
   */
  public function getProperties(): array {
    return $this->properties;
  }

}
