<?php

declare(strict_types=1);

namespace Drupal\Tests\rabbitmq\Unit\Serialization;

use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\rabbitmq\Exception\InvalidSerializerException;
use Drupal\rabbitmq\Serialization\Json;
use Drupal\rabbitmq\Serialization\PhpSerialize;
use Drupal\rabbitmq\Serialization\SerializerCollector;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the Json serializer.
 *
 * @group rabbitmq
 * @covers \Drupal\rabbitmq\Serialization\SerializerCollector
 */
class SerializerCollectorTest extends UnitTestCase {

  /**
   * Obtain the serializer under test.
   *
   * @return \Drupal\rabbitmq\Serialization\SerializerCollector
   *   The serializer under test
   */
  public function getFixture(): SerializerCollector {
    $class_resolver_mock = $this->createMock(ClassResolverInterface::class);
    $class_resolver_mock->method('getInstanceFromDefinition')->willReturnMap(
      [
        ['rabbitmq.serialization.phpserialize', new PhpSerialize()],
        ['rabbitmq.serialization.json', new Json()],
        ['not_a_serializer', new \stdClass()],
      ]
    );

    $list_of_services = [
      'rabbitmq.serialization.phpserialize',
      'rabbitmq.serialization.json',
      'not_a_serializer',
    ];
    return new SerializerCollector($class_resolver_mock, $list_of_services);
  }

  /**
   * Tes the getSerializer() method.
   *
   * @param non-empty-string $name
   *   ServiceId of the serializer to request.
   * @param class-string $expected_class
   *   Expected class of the serializer.
   * @param callable|null $setup
   *   Optional callable to make changes to the test. Provided $this as its
   *   first parameter.
   *
   * @dataProvider providerGetSerializer
   */
  public function testGetSerializer(string $name, string $expected_class, ?callable $setup = NULL): void {
    $service = $this->getFixture();
    if ($setup) {
      $setup($this);
    }
    $serializer = $service->getSerializer($name);
    $this->assertInstanceOf($expected_class, $serializer);

  }

  /**
   * DataProvider for testGetSerialzer()
   *
   * @return \Generator
   *   The test data.
   */
  public static function providerGetSerializer(): \Generator {

    yield 'Serializer php' => [
      'name' => 'rabbitmq.serialization.phpserialize',
      'expected_class' => PhpSerialize::class,
    ];

    yield 'Serializer json' => [
      'name' => 'rabbitmq.serialization.json',
      'expected_class' => Json::class,
    ];

    yield 'Serializer Doesnt Exist' => [
      'name' => 'doesnt_exist',
      'expected_class' => "\DoesNotExistClass",
      'setup' => function (self $context) {
        $context->expectException(InvalidSerializerException::class);
        $context->expectExceptionMessage('Unknown Serializer');
      },
    ];

    yield 'Class does not implement Serialization interface' => [
      'name' => 'not_a_serializer',
      'expected_class' => "\stdClass",
      'setup' => function (self $context) {
        $context->expectException(InvalidSerializerException::class);
        $context->expectExceptionMessage('Does not implement SerializationInterface');
      },
    ];

  }

}
