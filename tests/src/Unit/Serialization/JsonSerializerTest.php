<?php

declare(strict_types=1);

namespace Drupal\Tests\rabbitmq\Unit\Serialization;

use Drupal\Component\Serialization\Exception\InvalidDataTypeException;
use Drupal\rabbitmq\Serialization\Json;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the Json serializer.
 *
 * @group rabbitmq
 * @covers \Drupal\rabbitmq\Serialization\Json
 */
class JsonSerializerTest extends UnitTestCase {

  /**
   * Obtain the serializer under test.
   *
   * @return \Drupal\rabbitmq\Serialization\Json
   *   The serializer under test
   */
  public function getFixture(): Json {
    return new Json();
  }

  /**
   * Test the serializer encode() method.
   *
   * @param mixed $data
   *   The data in raw(PHP) form.
   * @param string $serialized_data
   *   The expected serialized string.
   * @param bool $assert_same
   *   Unused.
   *
   * @dataProvider providerSerializedData
   */
  public function testEncode(mixed $data, string $serialized_data, bool $assert_same = TRUE): void {
    $serializer = $this->getFixture();
    $result = $serializer->encode($data);
    $this->assertSame($serialized_data, $result);
  }

  /**
   * Test the serializer decode() method.
   *
   * @param mixed $data
   *   The expected data from de-serialization.
   * @param string $serialized_data
   *   The serialized string.
   * @param bool $assert_same
   *   If TRUE assertSame for $data, if FALSE
   *   assertEquals(). Necessary for testing objects.
   *
   * @dataProvider providerSerializedData
   */
  public function testDecode(mixed $data, string $serialized_data, bool $assert_same = TRUE): void {
    $serializer = $this->getFixture();
    $result = $serializer->decode($serialized_data);

    if ($assert_same) {
      $this->assertSame($data, $result);
    }
    else {
      $this->assertEquals($data, $result);
    }
  }

  /**
   * Provider for serialization tests.
   */
  public static function providerSerializedData(): \Generator {

    yield 'String' => [
      'data' => 'test string',
      'serialized_data' => '"test string"',
    ];

    yield 'Integer' => [
      'data' => 12345,
      'serialized_data' => '12345',
    ];

    yield 'Null' => [
      'data' => NULL,
      'serialized_data' => 'null',
    ];

    yield 'True' => [
      'data' => TRUE,
      'serialized_data' => 'true',
    ];

    yield 'False' => [
      'data' => FALSE,
      'serialized_data' => 'false',
    ];

    yield 'Array' => [
      'data' => ['foo' => 'bar'],
      'serialized_data' => '{"foo":"bar"}',
    ];

  }

  /**
   * Decode failure should throw an exception.
   */
  public function testDecodeException(): void {
    $serializer = $this->getFixture();
    $this->expectException(InvalidDataTypeException::class);
    $serializer->decode('O:40:"not json":0:{}');
    $this->fail('Expected exception not thrown');
  }

  /**
   * Test the getFileExtension() method.
   *
   * We do not use this for RabbitMq however it is a part of the interface.
   */
  public function testGetFileExtension(): void {
    $serializer = $this->getFixture();
    $this->assertSame('json', $serializer->getFileExtension());
  }

}
