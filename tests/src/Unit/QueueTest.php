<?php

namespace Drupal\Tests\rabbitmq\Unit;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Serialization\Exception\InvalidDataTypeException;
use Drupal\Component\Serialization\SerializationInterface;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\rabbitmq\ConnectionFactory;
use Drupal\rabbitmq\Exception\InvalidSerializerException;
use Drupal\rabbitmq\Queue\Queue;
use Drupal\rabbitmq\Serialization\Json;
use Drupal\rabbitmq\Serialization\PhpSerialize;
use Drupal\rabbitmq\Serialization\SerializerCollectorInterface;
use Drupal\Tests\UnitTestCase;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AbstractConnection;
use PhpAmqpLib\Message\AMQPMessage;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;

/**
 * Tests the queue class.
 *
 * @group rabbitmq
 *
 * @coversDefaultClass \Drupal\rabbitmq\Queue\Queue
 */
class QueueTest extends UnitTestCase {

  /**
   * Mock AMPQChannel.
   *
   * @var \PhpAmqpLib\Channel\AMQPChannel|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $channelMock;

  /**
   * Mock Connection.
   *
   * @var \PhpAmqpLib\Connection\AbstractConnection|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $connectionMock;

  /**
   * Mock connection factory.
   *
   * @var \Drupal\rabbitmq\ConnectionFactory|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $connectionFactoryMock;

  /**
   * Mock module_handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $moduleHandlerMock;

  /**
   * Logger service mock.
   *
   * @var \PHPUnit\Framework\MockObject\MockObject|\Psr\Log\LoggerInterface
   */
  protected $loggerMock;

  /**
   * Mock queue config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $configMock;

  /**
   * Mock UUID service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $uuidMock;

  /**
   * Mock time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $timeMock;

  /**
   * The SerializerCollector mock.
   *
   * @var \Drupal\rabbitmq\Serialization\SerializerCollectorInterface&\PHPUnit\Framework\MockObject\MockObject
   */
  protected SerializerCollectorInterface&MockObject $serializerCollectorMock;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // RabbitMQ Connection/Channel basic mocking.
    $this->channelMock = $this->createMock(AMQPChannel::class);
    $this->connectionMock = $this->createMock(AbstractConnection::class);
    $this->connectionMock->method('getConnection')->willReturnSelf();
    $this->connectionMock->method('channel')->willReturn($this->channelMock);
    $this->connectionFactoryMock = $this->createMock(ConnectionFactory::class);
    $this->connectionFactoryMock->method('getConnection')->willReturn($this->connectionMock);

    // Additional services required by the Queue class.
    $this->moduleHandlerMock = $this->createMock(ModuleHandlerInterface::class);
    $this->loggerMock = $this->createMock(LoggerInterface::class);
    $this->configMock = $this->createMock(ImmutableConfig::class);

    $this->uuidMock = $this->createMock(UuidInterface::class);
    $this->uuidMock->method('generate')->willReturn('00643728-8a9e-48bb-b337-db14ad1f3f2c');

    $this->timeMock = $this->createMock(TimeInterface::class);
    $this->timeMock->method('getCurrentTime')->willReturn(1600000000);

    $this->serializerCollectorMock = $this->createMock(SerializerCollectorInterface::class);
    $this->serializerCollectorMock->method('getSerializer')->willReturnCallback(
      function (string $serializer) {
        switch ($serializer) {

          case 'rabbitmq.serialization.phpserialize':
            return new PhpSerialize();

          case 'rabbitmq.serialization.json':
            return new Json();

          case 'rabbitmq.serialization.encode_decode_exception':
            return new class implements SerializationInterface {

              /**
               * {@inheritdoc}
               */
              public static function encode($data): string {
                throw new InvalidDataTypeException('String was unserializable.');
              }

              /**
               * {@inheritdoc}
               */
              public static function decode($raw): string {
                throw new InvalidDataTypeException('String was unserializable.');
              }

              /**
               * {@inheritdoc}
               */
              public static function getFileExtension(): string {
                return 'INVALID';
              }

            };

          case 'rabbitmq.serialization.unknown_serializer':
            throw new InvalidSerializerException();

          default:
            throw new \Exception('Mock not implemented');
        }
      }
    );

  }

  /**
   * Test that createItem handles item_id.
   *
   * @covers ::createItem
   */
  public function testCreateItemId() {
    $data = 'foo';

    $submitted_message = NULL;

    $this->channelMock
      ->expects($this->once())
      ->method('basic_publish')
      ->with(
        self::callback(
          function ($message) use (&$submitted_message): bool {
            $submitted_message = $message;
            return TRUE;
          }
        )
      );

    $queue = $this->getQueue();

    $item_id = $queue->createItem($data);
    $this->assertSame('00643728-8a9e-48bb-b337-db14ad1f3f2c', $submitted_message->get('message_id'), "Message has message_id header");
    $this->assertSame('00643728-8a9e-48bb-b337-db14ad1f3f2c', $item_id, "createItem returns correct item_id");
  }

  /**
   * Test that claimItem returns message created timestamp.
   *
   * @param string|\Exception $return_value
   *   Response to return for $msg->get('message_id')
   * @param string $expected_id
   *   Expected item_id value.
   *
   * @dataProvider itemIdDataProvider
   *
   * @covers ::claimItem
   */
  public function testItemIdFromClaimItem($return_value, string $expected_id) {
    // Mock a response message.
    $callback = function ($parameter) use ($return_value) {
      if ($parameter == 'message_id') {
        if ($return_value instanceof \Exception) {
          throw $return_value;
        }
        return $return_value;
      }
      else {
        throw new \OutOfBoundsException();
      }
    };

    $message_mock = $this->createMock(AMQPMessage::class);
    $message_mock->method('get')->willReturnCallback($callback);
    $message_mock->method('getDeliveryTag')->willReturn(1);
    $message_mock->body = serialize('foo');
    $this->channelMock->method('basic_get')->willReturn($message_mock);

    $queue = $this->getQueue();

    $item = $queue->claimItem();
    $this->assertNotFalse($item);
    $this->assertEquals($expected_id, $item->item_id);
  }

  /**
   * DataProvider for testItemIdFromClaimItem().
   *
   * @return array
   *   Array of test data.
   */
  public function itemIdDataProvider(): array {
    return [
      'No message_id set' => [
        new \OutOfBoundsException(),
        '00643728-8a9e-48bb-b337-db14ad1f3f2c',
      ],
      'With message_id' => [
        '38e14be6-6e21-4992-9eec-c1624f5f783b',
        '38e14be6-6e21-4992-9eec-c1624f5f783b',
      ],
      'With NULL message_id' => [
        NULL,
        '00643728-8a9e-48bb-b337-db14ad1f3f2c',
      ],
    ];
  }

  /**
   * Test that createItem returns message created timestamp.
   *
   * @covers ::createItem
   */
  public function testCreateItemTimestamp() {
    $submitted_message = NULL;
    $data = 'foo';

    $this->channelMock
      ->expects($this->once())
      ->method('basic_publish')
      ->with(
        self::callback(
          function ($message) use (&$submitted_message): bool {
            $submitted_message = $message;
            return TRUE;
          }
        )
      );

    $queue = $this->getQueue();

    $queue->createItem($data);
    $this->assertEquals(1600000000, $submitted_message->get('timestamp'), "Message has timestamp");
  }

  /**
   * Test that claimItem returns message created timestamp.
   *
   * @param string|\Exception $return_value
   *   Response to return for $msg->get('timestamp')
   * @param int $expected_timestamp
   *   Expected created timestamp value.
   *
   * @dataProvider timestampDataProvider
   *
   * @covers ::claimItem
   */
  public function testClaimItemTimestamp($return_value, int $expected_timestamp) {
    // Mock a response message.
    $callback = function ($parameter) use ($return_value) {
      if ($parameter == 'timestamp') {
        if ($return_value instanceof \Exception) {
          throw $return_value;
        }
        return $return_value;
      }
      else {
        throw new \OutOfBoundsException();
      }
    };
    $message_mock = $this->createMock(AMQPMessage::class);
    $message_mock->method('get')->willReturnCallback($callback);
    $message_mock->method('getDeliveryTag')->willReturn(1);
    $message_mock->body = serialize('foo');
    $this->channelMock->method('basic_get')->willReturn($message_mock);

    $queue = $this->getQueue();

    $item = $queue->claimItem();
    $this->assertNotFalse($item);
    $this->assertEquals($expected_timestamp, $item->created);
  }

  /**
   * DataProvider for testClaimItemTimestamp().
   *
   * @return array
   *   Array of test data.
   */
  public function timestampDataProvider(): array {
    return [
      'No timestamp set' => [
        new \OutOfBoundsException(),
        0,
      ],
      'With timestamp' => [
        1600000000,
        1600000000,
      ],
      'With NULL timestamp' => [
        NULL,
        0,
      ],
    ];
  }

  /**
   * Test that claimItem processes serializer selection.
   *
   * @param string $serialized_message
   *   The serialized message. Must be equivalent to (int) 12345.
   * @param ?string $serializer_name
   *   The name of the serializer to test with.
   * @param ?callable $setup
   *   Optional callable to make changes to the test. Provided $this as its
   *   first parameter.
   *
   * @dataProvider providerSerializerSelection
   *
   * @covers ::claimItem
   */
  public function testSerializerSelectionClaimItem(string $serialized_message, ?string $serializer_name = NULL, ?callable $setup = NULL): void {
    if ($serializer_name) {
      $this->configMock->method('get')->willReturnMap(
        [
          [
            'queues',
            [
              'unittest_queue' => [
                'name' => 'unittest_queue',
                'serializer' => $serializer_name,
              ],
            ],
          ],
        ]
      );
    }
    $message_mock = $this->createMock(AMQPMessage::class);
    $message_mock->body = $serialized_message;
    $this->channelMock->method('basic_get')->willReturn($message_mock);

    if ($setup !== NULL) {
      $setup($this);
    }

    $queue = $this->getQueue();

    /** @var false|object{'data': mixed} $item */
    $item = $queue->claimItem();
    $this->assertNotFalse($item);
    $this->assertSame(12345, $item->data);
  }

  /**
   * Test that createItem processes serializer selection.
   *
   * @param string $serialized_message
   *   The serialized message. Must be equivalent to (int) 12345.
   * @param ?string $serializer_name
   *   The name of the serializer to test with.
   * @param ?callable $setup
   *   Optional callable to make changes to the test. Provided $this as its
   *   first parameter.
   *
   * @dataProvider providerSerializerSelection
   *
   * @covers ::createItem
   */
  public function testSerializerSelectionCreateItem(string $serialized_message, ?string $serializer_name = NULL, ?callable $setup = NULL): void {
    $submitted_message = NULL;
    $data = 12345;

    if ($serializer_name) {
      $this->configMock->method('get')->willReturnMap(
        [
          [
            'queues',
            [
              'unittest_queue' => [
                'name' => 'unittest_queue',
                'serializer' => $serializer_name,
              ],
            ],
          ],
        ]
      );
    }

    $this->channelMock
      ->method('basic_publish')
      ->with(
        self::callback(
          function ($message) use (&$submitted_message): bool {
            $submitted_message = $message;
            return TRUE;
          }
        )
      );

    if ($setup !== NULL) {
      $setup($this);
    }

    $queue = $this->getQueue();

    $queue->createItem($data);
    $this->assertSame($serialized_message, $submitted_message->body, "Message body correctly serialized");
  }

  /**
   * DataProvider for testing serializer selection.
   *
   * @return \Generator
   *   Test Data.
   */
  public static function providerSerializerSelection(): \Generator {

    yield 'Default to PHP serialization' => [
      'serialized_message' => 'i:12345;',
      'serializer_name' => NULL,
    ];

    yield 'Specify PHP serialization' => [
      'serialized_message' => 'i:12345;',
      'serializer_name' => 'rabbitmq.serialization.phpserialize',
    ];

    yield 'Specify JSON serialization' => [
      'serialized_message' => '12345',
      'serializer_name' => 'rabbitmq.serialization.json',
    ];

    yield 'Serialization Failure' => [
      'serialized_message' => '12345',
      'serializer_name' => 'rabbitmq.serialization.encode_decode_exception',
      'setup' => function (self $context) {
        $context->expectException(InvalidDataTypeException::class);
      },
    ];

    yield 'Unknown serializer' => [
      'serialized_message' => '12345',
      'serializer_name' => 'rabbitmq.serialization.unknown_serializer',
      'setup' => function (self $context) {
        $context->expectException(InvalidSerializerException::class);
      },
    ];

  }

  /**
   * Helper to obtain a Queue using the mocked services.
   *
   * @return \Drupal\rabbitmq\Queue\Queue
   *   A queue using the mocks previously configured.
   */
  protected function getQueue(): Queue {
    return new Queue(
      'unittest_queue',
      $this->connectionFactoryMock,
      $this->moduleHandlerMock,
      $this->loggerMock,
      $this->configMock,
      $this->uuidMock,
      $this->timeMock,
      $this->serializerCollectorMock,
    );
  }

}
