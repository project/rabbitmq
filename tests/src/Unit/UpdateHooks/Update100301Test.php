<?php

declare(strict_types=1);

namespace Drupal\Tests\rabbitmq\Unit;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Tests\UnitTestCase;
use PHPUnit\Framework\MockObject\MockObject;

require_once __DIR__ . '/../../../../rabbitmq.install';

/**
 * Tests rabbitmq_update_100301()
 *
 * @group rabbitmq
 *
 * @covers ::\rabbitmq_update_100301()
 */
class Update100301Test extends UnitTestCase {

  /**
   * The ConfigFactory mock.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface&\PHPUnit\Framework\MockObject\MockObject
   */
  protected ConfigFactoryInterface&MockObject $configFactoryMock;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->configFactoryMock = $this->getConfigFactoryStub(
      [
        'rabbitmq.config' => [
          'queues' => [
            'queue_json' => [
              'name' => 'queue_json',
              'serializer' => 'rabbitmq.serialization.json',
            ],
            'queue_php' => [
              'name' => 'queue_php',
              'serializer' => 'rabbitmq.serialization.phpserialize',
            ],
            'queue_no_serializer' => [
              'name' => 'queue_no_serializer',
            ],
          ],
        ],
      ]
    );

    $container = new ContainerBuilder();
    $container->set('config.factory', $this->configFactoryMock);
    \Drupal::setContainer($container);
  }

  /**
   * Test default serializer set.
   */
  public function testUpdate100301(): void {
    /** @var \Drupal\Core\Config\Config&MockObject $config */
    $config = $this->configFactoryMock->getEditable('rabbitmq.config');
    $config
      ->expects($this->once())
      ->method('set')
      ->with(
        'queues',
        [
          'queue_json' => [
            'name' => 'queue_json',
            'serializer' => 'rabbitmq.serialization.json',
          ],
          'queue_php' => [
            'name' => 'queue_php',
            'serializer' => 'rabbitmq.serialization.phpserialize',
          ],
          'queue_no_serializer' => [
            'name' => 'queue_no_serializer',
            'serializer' => 'rabbitmq.serialization.phpserialize',
          ],
        ],
      );
    $config->expects($this->once())->method('save');

    rabbitmq_update_100301();
  }

}
