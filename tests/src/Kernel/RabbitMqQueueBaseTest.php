<?php

namespace Drupal\Tests\rabbitmq\Kernel;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigException;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;
use Drupal\Core\Site\Settings;
use Drupal\rabbitmq\Queue\Queue;
use Drupal\rabbitmq\Queue\QueueItemWithProperties;

/**
 * Class RabbitMqQueueTest.
 *
 * @group RabbitMQ
 */
class RabbitMqQueueBaseTest extends RabbitMqTestBase implements ServiceModifierInterface {

  /**
   * The default queue, handled by RabbitMq.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * The queue factory service.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $time_mock = $this->createMock(TimeInterface::class);
    $time_mock->method('getCurrentTime')->willReturn(1600000000);
    $container->set('datetime.time', $time_mock);
  }

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->queueFactory = $this->container->get('queue');
    $this->queue = $this->queueFactory->get($this->queueName);
    $this->assertInstanceOf(Queue::class, $this->queue, 'Queue API settings point to RabbitMQ');
    $this->queue->createQueue();
  }

  /**
   * {@inheritdoc}
   */
  public function tearDown(): void {
    $this->queue->deleteQueue();
    parent::tearDown();
  }

  /**
   * Test RabbitMqServiceProvider registers services for each connection.
   */
  public function testServiceExistsForCredentials(): void {

    // Add another set of credentials.
    $settings = Settings::getAll();
    $settings['rabbitmq_credentials']['invalid'] = [
      'host' => 'rabbitmq',
      'port' => 5672,
      'vhost' => '/',
      'username' => 'not_valid',
      'password' => 'not_valid',
    ];
    new Settings($settings);

    // RabbitmqServiceProvider requires a container rebuild to process the
    // settings configured above.
    /** @var \Drupal\Core\DrupalKernelInterface $kernel */
    $kernel = $this->container->get('kernel');
    $kernel->invalidateContainer();
    $kernel->rebuildContainer();

    // Phpstan-drupal can't discover dynamic services so ignore errors.
    // @phpstan-ignore-next-line
    $this->assertTrue($this->container->has('rabbitmq.connection.factory.default'));
    // @phpstan-ignore-next-line
    $this->assertTrue($this->container->has('queue.rabbitmq.default'));
    // @phpstan-ignore-next-line
    $this->assertTrue($this->container->has('rabbitmq.connection.factory.invalid'));
    // @phpstan-ignore-next-line
    $this->assertTrue($this->container->has('queue.rabbitmq.invalid'));
    $this->assertFalse($this->container->has('rabbitmq.connection.factory.nonexistent'));
    $this->assertFalse($this->container->has('queue.rabbitmq.default.nonexistent'));
  }

  /**
   * Test queue registration.
   */
  public function testQueueCycle(): void {
    $data = 'foo';
    $this->queue->createItem($data);
    // We call numberOfItems() twice during testing to ensure an accurate value.
    $this->queue->numberOfItems();
    $actual = $this->queue->numberOfItems();
    $expected = 1;
    $this->assertEquals($expected, $actual, 'Queue contains something before deletion');

    $this->queue->deleteQueue();
    $expected = 0;
    // Queue deleted already, calling twice will throw an exception.
    $actual = $this->queue->numberOfItems();
    $this->assertEquals($expected, $actual, 'Queue no longer contains anything after deletion');
  }

  /**
   * Test queue priority.
   */
  public function testQueuePriority() {
    $config = $this->config('rabbitmq.config');
    $queues = $config->get('queues');
    $queues[$this->queueName] = [
      'passive' => FALSE,
      'durable' => TRUE,
      'exclusive' => FALSE,
      'auto_delete' => FALSE,
      'nowait' => FALSE,
      'routing_keys' => [],
      'arguments' => ['x-max-priority' => ['I', 10]],
    ];

    $config->set('queues', $queues)->save();

    $this->queue = $this->queueFactory->get($this->queueName);
    $this->queue->createQueue();

    // Message with a priority.
    $this->queue->createItem(new QueueItemWithProperties(['priority' => 5], ['priority' => 5]));

    // Message with a different priority.
    $this->queue->createItem(new QueueItemWithProperties(['priority' => 3], ['priority' => 3]));

    // Message with empty MessageArgs.
    $this->queue->createItem(new QueueItemWithProperties([], []));

    /** @var \PhpAmqpLib\Channel\AMQPChannel $channel */
    $channel = $this->connectionFactory->getConnection()->channel();

    $number_of_messages = 0;

    // We use the message body to indicate what keys we expect
    // to be present in the message properties.
    while ($message = $channel->basic_get($this->queueName)) {
      $properties = $message->get_properties();
      $expected_properties = unserialize($message->getBody());
      $this->assertIsArray($expected_properties);
      foreach ($expected_properties as $key => $expected_value) {
        $this->assertArrayHasKey($key, $properties, "$key present in message properties");
        $this->assertEquals($expected_value, $properties[$key], "$key matches expected properties");
      }
      $number_of_messages++;
    }

    // We expect to receive three messages. Note that calling
    // $this->queue->numberOfItems() provides a "best guess" of the number of
    // items in the queue. It is not guaranteed to be accurate. Therefore, we
    // count the number of messages we receive ourselves.
    $this->assertEquals(3, $number_of_messages, 'All messages were received');
  }

  /**
   * Test the queue item lifecycle.
   */
  public function testItemCycle(): void {
    $count = 0;
    $data = 'foo';
    $this->queue->createItem($data);

    // We call numberOfItems() twice during testing to ensure an accurate value.
    $this->queue->numberOfItems();
    $actual = $this->queue->numberOfItems();
    $expected = $count + 1;
    $this->assertEquals($expected, $actual, 'Creating an item increases the item count.');

    $item = $this->queue->claimItem();
    $this->assertTrue(is_object($item), 'Claiming returns an item');

    $expected = $data;
    $actual = $item->data;
    $this->assertEquals($expected, $actual, 'Item content matches submission.');

    // We call numberOfItems() twice during testing to ensure an accurate value.
    $this->queue->numberOfItems();
    $actual = $this->queue->numberOfItems();
    $expected = $count;
    $this->assertEquals($expected, $actual, 'Claiming an item reduces the item count.');

    $this->queue->releaseItem($item);
    // We call numberOfItems() twice during testing to ensure an accurate value.
    $this->queue->numberOfItems();
    $actual = $this->queue->numberOfItems();
    $expected = $count + 1;
    $this->assertEquals($expected, $actual, 'Releasing an item increases the item count.');

    $item = $this->queue->claimItem();
    $this->assertTrue(is_object($item), 'Claiming returns an item');

    $this->queue->deleteItem($item);
    // We call numberOfItems() twice during testing to ensure an accurate value.
    $this->queue->numberOfItems();
    $actual = $this->queue->numberOfItems();
    $expected = $count;
    $this->assertEquals($expected, $actual, 'Deleting an item reduces the item count.');
  }

  /**
   * Validate config for queues can be saved.
   *
   * Validates that all keys in the rabbitmq.schema.yml file are correct.
   */
  public function testQueueConfigSave() {
    $config_factory = \Drupal::configFactory()->getEditable('rabbitmq.config');

    $queues = $config_factory->get('queues');

    $queues[] = [
      'auto_delete' => TRUE,
      'durable' => FALSE,
      'exclusive' => TRUE,
      'name' => 'test.queue',
      'nowait' => FALSE,
      'passive' => TRUE,
      'routing_keys' => [
        "exchange1.test_queue",
      ],
      'arguments' => [
        'alpha' => [1, 2],
        'beta' => 'value',
      ],
      'ticket' => 12345,
    ];

    $config = $config_factory->set('queues', $queues)->save(TRUE);
    $this->assertIsObject($config, 'Config save returned an object without exception');
  }

  /**
   * Test that an exception is thrown when a queue name is not configured.
   */
  public function testMissingQueueNameException() {
    $this->expectException(ConfigException::class);
    $this->expectExceptionMessage('Queue name is missing in configuration.');

    // Set queue configuration without a name.
    $config = \Drupal::configFactory()->getEditable('rabbitmq.config');
    $queues['missing_name_queue'] = [
      'auto_delete' => TRUE,
      'durable' => FALSE,
      'exclusive' => TRUE,
      'nowait' => FALSE,
      'passive' => TRUE,
      'routing_keys' => [
        "exchange1.test_queue",
      ],
      'arguments' => [
        'alpha' => [1, 2],
        'beta' => 'value',
      ],
      'ticket' => 12345,
    ];
    $config->set('queues', $queues)->save();

    // Get a queue to initiate the process to use the configuration.
    $this->queueFactory->get('missing_name_queue');
  }

  /**
   * Test that claimItem returns a matching UUID for 'item_id' property.
   *
   * @todo Eventually validate ALL the return object properties in one test.
   */
  public function testItemId() {
    $data = 'foo';
    $expected_id = $this->queue->createItem($data);
    $this->assertNotFalse($expected_id);
    $item = $this->queue->claimItem();
    $this->assertIsString($item->item_id, "Message item_id is string");
    $this->assertEquals($expected_id, $item->item_id, 'Message item_id matches claimItem return');
  }

  /**
   * Test that claimItem returns message created timestamp.
   *
   * @todo Eventually validate ALL the return object properties in one test.
   */
  public function testTimestamp() {
    $data = 'foo';
    $this->queue->createItem($data);
    $item = $this->queue->claimItem();
    $this->assertIsInt($item->created, "Created timestamp is integer");
    $this->assertEquals(1600000000, $item->created, 'Created timestamp is expected value');
  }

  /**
   * Test that an object submitted to createItem is returned by claimItem.
   *
   * @todo Eventually validate ALL the return object properties in one test.
   */
  public function testCreateAndClaimObjects() {
    $data = new \stdClass();
    $data->submitedProperty = 'foo';

    $this->queue->createItem($data);
    $item = $this->queue->claimItem();
    $this->assertEquals($data, $item->data, 'Objects are processed as is by the queue.');
  }

}
