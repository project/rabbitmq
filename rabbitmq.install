<?php

/**
 * @file
 * Installer for the RabbitMQ module.
 */

use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use Drupal\rabbitmq\ConnectionFactory;
use Drupal\rabbitmq\Consumer;
use Drupal\rabbitmq\Queue\QueueBase;
use Drupal\rabbitmq\Queue\QueueFactory;

/**
 * Implements hook_requirements().
 *
 * @link https://www.drupal.org/node/2681929 @endlink
 */
function rabbitmq_requirements(string $phase): array {
  // During the install phase, the autoloader cannot yet load module classes,
  // so load them manually. Do not register them on the autoloader in case the
  // install files.
  if ($phase !== 'runtime') {
    require_once __DIR__ . '/src/ConnectionFactory.php';
    require_once __DIR__ . '/src/Queue/QueueBase.php';
    require_once __DIR__ . '/src/Queue/Queue.php';
    require_once __DIR__ . '/src/Queue/QueueFactory.php';
    require_once __DIR__ . '/src/Consumer.php';
  }

  $key = QueueBase::MODULE;
  $req = [
    $key => [
      'title' => t('RabbitMQ'),
    ],
  ];

  Consumer::hookRequirements($phase, $req);

  if ($phase === 'runtime') {
    // Routes are not know during install.
    $url = Url::fromRoute('rabbitmq.properties')->toString();
    $req[$key]['description'] = t('RabbitMQ connection information. See <a href=":link">RabbitMQ properties</a>.', [
      ':link' => $url,
    ]);
    $req[$key]['severity'] = REQUIREMENT_INFO;
  }

  $credentials = Settings::get(ConnectionFactory::CREDENTIALS);
  if (empty($credentials)) {
    $req[$key]['description'] = t('RabbitMQ credentials not found in settings.php');
    $req[$key]['severity'] = REQUIREMENT_WARNING;
    return $req;
  };

  // Module-defined services are not available during install.
  if ($phase === 'runtime') {
    $factory = \Drupal::service('rabbitmq.connection.factory');
    try {
      $factory->getConnection();
    }
    catch (Exception $e) {
      $req[$key]['value'] = t('Could not connect to RabbitMQ: "@message".', [
        '@message' => $e->getMessage(),
      ]);
      $req[$key]['severity'] = REQUIREMENT_WARNING;
      // If RabbitMQ is set as the default queue service, remove it to avoid
      // breakage in hook_requirements() implementations depending on that
      // default queue, like update_requirements().
      if (Settings::get('queue_default') == 'queue.rabbitmq') {
        QueueFactory::overrideSettings();
        $req[$key]['description'] = t('The default queue service pointing to queue.rabbitmq in settings has been reset to its default to avoid breaking requirements checks depending on the queue service, like update_requirements(), since the RabbitMQ queue is not actually available.');
      }
      return $req;
    }
  }

  return $req;
}

/**
 * Set queue and exchange name property into the configuration.
 *
 * @see https://www.drupal.org/project/rabbitmq/issues/3349968
 */
function rabbitmq_update_9401() {
  $config = \Drupal::configFactory()->getEditable('rabbitmq.config');

  // Process all configured queues, and use the queue's key as value for the
  // name if no name property has been set yet.
  $queues = [];
  foreach ($config->get('queues') as $name => $options) {
    if (empty($options['name'])) {
      $options['name'] = $name;
    }

    $queues[] = $options;
  }
  $config->set('queues', $queues);

  // Process all configured exchanges, and use the exchange's key as value for
  // the name if no name property has been set yet.
  $exchanges = [];
  foreach ($config->get('exchanges') as $name => $options) {
    if (empty($options['name'])) {
      $options['name'] = $name;
    }

    $exchanges[] = $options;
  }
  $config->set('exchanges', $exchanges);

  $config->save();
}

/**
 * Set serializer property into the configuration.
 *
 * @see https://www.drupal.org/node/3420913
 */
function rabbitmq_update_100301(): void {
  $config = \Drupal::configFactory()->getEditable('rabbitmq.config');

  $existing_queues = $config->get('queues');
  if (!is_array($existing_queues) || empty($existing_queues)) {
    return;
  }

  // Process all configured queues, Set default serializer to PHP if not set.
  $new_queues = [];
  foreach ($existing_queues as $name => $options) {
    if (!isset($options['serializer'])) {
      $options['serializer'] = 'rabbitmq.serialization.phpserialize';
    }

    $new_queues[$name] = $options;
  }
  $config->set('queues', $new_queues);
  $config->save();
}
